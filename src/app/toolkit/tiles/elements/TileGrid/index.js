
/**
 * -----------------------------------------------------------------------------
 * Imports
 * -----------------------------------------------------------------------------
 */
import React, { Component, Fragment } from 'react';
import TweenMax from 'gsap';


/**
 * -----------------------------------------------------------------------------
 * React Component: TileGrid
 * -----------------------------------------------------------------------------
 */

class TileGrid extends Component {

    static dependencies() {
        return (typeof module !== 'undefined') ? module.children : [];
    }

    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.props);
        this.tiles = [];
    }

    componentDidMount() {
        // this.tiles.forEach((item) => {
        //     let idx = item.dataset.index;
        //     let back = document.getElementById(`tile-back-${idx}`);
        //     TweenMax.set(back, {rotationY:-180});
        // });
    }

    componentWillReceiveProps(nextProps) {
        this.setState((prevState) => {
            return Object.assign({}, prevState, nextProps);
        });
    }

    flipBack(e) {
        // let elm = e.currentTarget;
        // let idx = elm.dataset.index;
        // let back = document.getElementById(`tile-back-${idx}`);
        // let front = document.getElementById(`tile-front-${idx}`);
        // TweenMax.to(elm, 1, {rotationY:-180});
        //TweenMax.to([back], 1, {rotationY:180});

        //let elm = e.currentTarget;
        // let idx = e.currentTarget.dataset.index;
        // let elm = document.getElementById(`tile-grid-inner-${idx}`);
        // let back = document.getElementById(`tile-back-${idx}`);
        // let front = document.getElementById(`tile-front-${idx}`);
        //
        // TweenMax.to(back, 1, {rotationY: '+=180'});
        // TweenMax.to(front, 1, {rotationY: '+=180'});
        //TweenMax.to(elm, 1, {rotationY: -180});
    }

    flipFront(e) {
        // let elm = e.currentTarget;
        // let idx = elm.dataset.index;
        // let back = document.getElementById(`tile-back-${idx}`);
        // let front = document.getElementById(`tile-front-${idx}`);
        // TweenMax.to(elm, 1, {rotationY:0});
        //TweenMax.to([back], 1, {rotationY:-180});

        //let elm = e.currentTarget;
        // let idx = e.currentTarget.dataset.index;
        // let elm = document.getElementById(`tile-grid-inner-${idx}`);
        // let back = document.getElementById(`tile-back-${idx}`);
        // let front = document.getElementById(`tile-front-${idx}`);
        //
        // TweenMax.to(back, 1, {rotationY: '-=180'});
        // TweenMax.to(front, 1, {rotationY: '-=180'});
        //TweenMax.to(elm, 1, {rotationY: 0});
    }

    registerTile(elm) {
        this.tiles.push(elm);
    }

    renderGrid(count = 36, colCount = 4) {
        let rowCount = Math.ceil(count/4);
        this.tiles = [];

        let idx = 0;
        let rows = [];
        for (let r = 1; r <= rowCount; r++) {
            let items = [];
            for (let i = 1; i <= colCount; i++) {
                let style = (idx === 1) ? {
                    backgroundImage: "url('https://pbs.twimg.com/profile_images/948294484596375552/RyGNqDEM_400x400.jpg')",
                    backgroundSize: 'contain',
                    backgroundRepeat: 'no-repeat',
                    cursor: 'pointer',
                } : {};
                items.push(
                    <div
                        data-index={idx}
                        className="tile-grid-cell"
                        key={`grid-item-${idx}`}
                        ref={(elm) => { this.registerTile(elm, idx); }}
                        onMouseOver={this.flipBack.bind(this)}
                        onMouseOut={this.flipFront.bind(this)}
                    >
                        <div className="tile-grid-inner" style={style}>
                            <div className="tile-grid-cont">
                                <span className="tile-grid-cont-front" id={`tile-front-${idx}`}>{idx} front</span>
                                <span className="tile-grid-cont-back"  id={`tile-back-${idx}`}>{idx} back</span>
                            </div>
                        </div>
                    </div>
                );
                idx += 1;
            }
            rows.push(items);
        }

        return rows.map((row, i) => {
            return (
                <div className="tile-grid-row" key={`grid-row-${i}`}>
                    {row.map((item) => {
                        return (item);
                    })}
                </div>
            );
        });
    }

    renderGridNew(count = 36) {
        let output = [];
        for (let i = 1; i <= 36; i++) {

            let style = (i === 5) ? {
                front: {
                    backgroundImage: 'url("https://pbs.twimg.com/profile_images/948294484596375552/RyGNqDEM_400x400.jpg")',
                    backgroundSize: 'contain',
                },
                back: {
                    backgroundColor: 'red',
                }
            } : {
                front: {
                    backgroundColor: '#CCC',
                },
                back: {
                    backgroundColor: '#CCC',
                }
            };

            output.push(
                <div
                    onMouseOut={this.flipFront.bind(this)}
                    onMouseOver={this.flipBack.bind(this)}
                    className={`tile-grid-cell`}
                    key={`tile-grid-item-${i}`}
                    data-index={i}
                    >
                    <div id={`tile-grid-inner-${i}`} className={`tile-grid-inner`}>
                        <div id={`tile-back-${i}`} className={`tile-grid-back`} style={style.back}>back {i}</div>
                        <div id={`tile-front-${i}`} className={`tile-grid-front`} style={style.front}>front {i}</div>
                    </div>
                </div>
            );
        }

        return output;
    }

    render() {
        return (
            <div className="tile-grid">
                {this.renderGrid()}
            </div>
        );
    }
}

// Default properties
TileGrid.defaultProps = {};

export default TileGrid;
