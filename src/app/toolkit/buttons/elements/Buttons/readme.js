import React from 'react';
import Markdown from 'reactium-core/components/Toolkit/Markdown';

/**
 * -----------------------------------------------------------------------------
 * ButtonOverview Readme
 * -----------------------------------------------------------------------------
 */

const content = `
Use the ${'`.btn-*`'} classes to modify button elements.


| Parameter  | Values | Example |
|------------|----|----|
| **Colors** | default / primary / secondary | ${'`.btn-secondary`'} |
| **Sizes**  | xs / sm / md / lg | ${'`.btn-primary-md`'} |
| **Styles** | pill | ${'`.btn-default-lg-pill`'} |
`;


/**
 * -----------------------------------------------------------------------------
 * DO NOT EDIT BELOW HERE
 * -----------------------------------------------------------------------------
 */
const readme = (props) => <Markdown {...props}>{content}</Markdown>;
export default readme;
