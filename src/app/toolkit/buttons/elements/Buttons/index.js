
/**
 * -----------------------------------------------------------------------------
 * Imports
 * -----------------------------------------------------------------------------
 */
import React, { Component, Fragment } from 'react';
import DefaultButton from '../DefaultButton';
import PrimaryButton from '../PrimaryButton';
import SecondaryButton from '../SecondaryButton';

/**
 * -----------------------------------------------------------------------------
 * React Component: ButtonOverview
 * -----------------------------------------------------------------------------
 */

class Buttons extends Component {

    static dependencies() {
        return (typeof module !== 'undefined') ? module.children : [];
    }

    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.props);
    }

    componentDidMount() {
        if (this.state.hasOwnProperty('mount')) {
            this.state.mount(this);
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState((prevState) => {
            return Object.assign({}, prevState, nextProps);
        });
    }

    render() {
        let btnStyleW = {
            margin: 10,
            minWidth: 180,
        };

        let btnStyle = {
            margin: 10,
        }

        let sizes = {
            lg: 'Large',
            md: 'Medium',
            sm: 'Small',
            xs: 'Extra Small'
        };

        let colors = {
            'default': DefaultButton,
            'primary': PrimaryButton,
            'secondary': SecondaryButton,
        };

        const clrs = Object.keys(colors);

        return (
            <div style={{margin: -10}}>
                <div>
                    {Object.keys(colors).map((color, i) => {
                        const Cmp = colors[color];

                        return (
                            <div key={`color-${i}`}>
                                <Cmp style={btnStyleW} />
                                <Cmp style={btnStyleW} className={`btn-${color}-pill`} label={`${color} Pill`} />
                            </div>
                        );
                    })}
                </div>

                {Object.keys(sizes).map((size, i) => {
                    let color = clrs.shift();
                    clrs.push(color);

                    let label = sizes[size];
                    const Cmp = colors[color];

                    return (
                        <Fragment key={`${color}-size-${i}`}>
                            <h4 className={'tk'}>{label}</h4>
                            <div>
                                <Cmp style={btnStyle} className={`btn-${color}-${size}`} />
                                <Cmp style={btnStyle} className={`btn-${color}-${size}-pill`} label={`${color} Pill`} />
                            </div>
                        </Fragment>
                    );
                })}
            </div>
        );
    }
}

// Default properties
Buttons.defaultProps = {};

const Button = DefaultButton;

export {
    Button,
    DefaultButton,
    PrimaryButton,
    SecondaryButton,
}

export default Buttons;
