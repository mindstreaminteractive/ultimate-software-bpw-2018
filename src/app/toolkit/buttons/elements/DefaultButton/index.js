
/**
 * -----------------------------------------------------------------------------
 * Imports
 * -----------------------------------------------------------------------------
 */
import React, { Component, Fragment } from 'react';


/**
 * -----------------------------------------------------------------------------
 * React Component: DefaultButton
 * -----------------------------------------------------------------------------
 */

class DefaultButton extends Component {

    static dependencies() {
        return (typeof module !== 'undefined') ? module.children : [];
    }

    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.setState((prevState) => {
            return Object.assign({}, prevState, nextProps);
        });
    }

    render() {
        let { children, label } = this.state;
        let newProps = Object.assign({}, this.state);
        delete newProps.label;
        delete newProps.children;

        return (
            <button className={'btn-default'} {...newProps}>
                {label}
                {children}
            </button>
        );
    }
}

// Default properties
DefaultButton.defaultProps = {
    label: 'Default Button',
};

export default DefaultButton;
