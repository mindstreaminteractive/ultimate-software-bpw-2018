
/**
 * -----------------------------------------------------------------------------
 * Imports
 * -----------------------------------------------------------------------------
 */
import React, { Component, Fragment } from 'react';
import DefaultButton from '../DefaultButton';


/**
 * -----------------------------------------------------------------------------
 * React Component: PrimaryButton
 * -----------------------------------------------------------------------------
 */

class PrimaryButton extends DefaultButton {
    static dependencies() {
        return (typeof module !== 'undefined') ? module.children : [];
    }
}

// Default properties
PrimaryButton.defaultProps = {
    label: 'Primary Button',
    className: 'btn-primary',
};

export default PrimaryButton;
