
/**
 * -----------------------------------------------------------------------------
 * Imports
 * -----------------------------------------------------------------------------
 */
import React, { Component, Fragment } from 'react';
import DefaultButton from '../DefaultButton';

/**
 * -----------------------------------------------------------------------------
 * React Component: SecondaryButton
 * -----------------------------------------------------------------------------
 */

class SecondaryButton extends DefaultButton {
    static dependencies() {
        return (typeof module !== 'undefined') ? module.children : [];
    }
}

// Default properties
SecondaryButton.defaultProps = {
    label: 'Secondary Button',
    className: 'btn-secondary',
};

export default SecondaryButton;
