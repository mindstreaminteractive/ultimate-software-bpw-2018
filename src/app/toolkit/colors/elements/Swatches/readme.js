import React from 'react';
import Markdown from 'reactium-core/components/Toolkit/Markdown';

/**
 * -----------------------------------------------------------------------------
 * Swatches Readme
 * -----------------------------------------------------------------------------
 */

const content = `
#### Background Color
By using ${'`bg-`'} + ${'`colorname`'} you can set the background color of an element.

#### Usage:
${'```'}
<div class="bg-pink">
    ...
</div>
${'```'}


#### Text Color
By using the color name on an element, you can set it's text color.

#### Usage:
${'```'}
<div class="pink">
    ...
</div>
${'```'}

> These classes will not override styles set with ${'`!important`'}.

`;


/**
 * -----------------------------------------------------------------------------
 * DO NOT EDIT BELOW HERE
 * -----------------------------------------------------------------------------
 */
const readme = (props) => <Markdown {...props}>{content}</Markdown>;
export default readme;
