
/**
 * -----------------------------------------------------------------------------
 * Imports
 * -----------------------------------------------------------------------------
 */
import React, { Component, Fragment } from 'react';
import copy from 'copy-to-clipboard';
import colors from './colors';

/**
 * -----------------------------------------------------------------------------
 * React Component: Swatches
 * -----------------------------------------------------------------------------
 */

class Swatches extends Component {

    static dependencies() {
        return (typeof module !== 'undefined') ? module.children : [];
    }

    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.props);
    }

    componentDidMount() {
        if (this.state.hasOwnProperty('mount')) {
            this.state.mount(this);
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState((prevState) => {
            return Object.assign({}, prevState, nextProps);
        });
    }

    onCopy({color, code}) {
        copy(code);
        alert(`Copied ${color} color code: ${code} to clipboard`);
    }

    swatches() {
        return Object.keys(colors).map((color, i) => {
            let code = colors[color];
            return (
                <div className={`col-xs-12 col-sm-6 col-md-4 col-lg-2`} key={`swatch-${i}`}>
                    <div className={`swatch bg-${color}`} onClick={() => { this.onCopy({code: code, color}); }} />
                    <div className={`swatch-info`}>{code} &ndash; <span className={`swatch-info-name`}>{color}</span></div>
                </div>
            );
        });
    }

    render() {
        return (
            <div className={`row swatches`}>
                {this.swatches()}
            </div>
        );
    }
}

// Default properties
Swatches.defaultProps = {};

export default Swatches;
